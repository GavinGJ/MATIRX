matrix manage   -gaojian

GJ01.管理页面（获得微服务）
http://127.0.0.1:8080/Matrix/manage/getApp.json
{
   
    "envId":"2",                            //环境id
    "userId":"1"                            //当前用户id
    "currentPage":"1",
    "pageCount":"10"
}

{
    "status": "0"                         // 成功 0-成功      1-不成功  
    "currentPage":"1",
    "data":[
        {
            "appId":"1",
            "appName":"微服务名字1"，
        },
        {
            "appId":"2",
            "appName":"微服务名字2"，
        }   
    ] 
}

GJ02.管理页面（获得版本号）
http://127.0.0.1:8080/Matrix/manage/getVersion.json
{
    "appId":"1"，                           //微服务id
    "envId":"2",                            //环境id
    "userId":"1"                            //当前用户id
    "currentPage":"1",
    "pageCount":"10"
}

{
    "status": "0"                         // 成功 0-成功      1-不成功  
    "currentPage":"1",
    "version":[
            "版本1"，
            "版本2"，
    ] 
}

GJ03.微服务管理(删除)
http://127.0.0.1:8080/Matrix/manage/microServicesDelete.json
{
    "envId":"1",                    //环境id
	"appId":"5",  					//微服务编号id
	"userId":"1",					//用户id
}

{
    "status": "0"                         // 成功 0-成功      1-不成功    
}


GJ04.微服务版本管理(删除)
http://127.0.0.1:8080/Matrix/manage/versionDelete.json
{
    "envId":"1",                    //环境id
    "appId":"5",                    //微服务编号id
	"version":"0.0.5",                  //版本
	"userId":"1"							//当前用户id
}

{
    "status": "0"                         // 成功 0-成功      1-不成功    
}

GJ05.复制某版本号下的全部文件
http://127.0.0.1:8080/Matrix/manage/copyProperties.json
{
    "versionNameCopySource":"1"，                   //复制版本源name
    "envIdCopySource":"1",                          //复制环境源id 
    "appIdCopySource":"1",                          //复制微服务源Id
    "versionNameTarget":"0.0.2"                     //复制版本目标name
    "envIdCopyTarget":"2",                          //复制环境目标Id
    "appIdCopyTarget":"2"                           //复制微服务目标Id

    "userId":"1"                            //当前用户id
}

{
    "status": "0"                         // 成功 0-成功      1-不成功    

}

