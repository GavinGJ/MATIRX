import org.aspectj.lang.JoinPoint;

public class LogTest {
    public void before(JoinPoint joinPoint) {
        joinPoint.getArgs();
        System.out.println("被拦截方法调用之前调用此方法，输出此语句");
    }

    public void after() {
        System.out.println("被拦截方法调用之后调用此方法，输出此语句");
    }
}
