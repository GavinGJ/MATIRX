

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.danlu.matrix.model.issue;

public class HttpClientPut {

    @Test
    public  void testUpdate() throws Exception {
        
        /**
         * 1.谁执行的操作
         * 2.在[？]环境[？]微服务[？]版本
         * 3.执行了什么操�?
         * 
         * admin (update/delete/add) ed  (some.properties) in (env,app,version) at (sometime)
         * 
         * updateRedmine(key,operation,properties,env,app,version,time);
         * 
         */

        
        JSONObject jo  = new JSONObject();
        issue issue = new issue();
        issue.setNotes("哎，中文终于可以了~~~~~~");
//        issue.setNotes("admin (update/delete/add) ed  (some.properties) in (env,app,version) at (sometime)");
        //修改主题  issue.setSubject("yike test");
        jo.put("issue", issue);
         
        String url = "http://182.92.230.201:3000/issues/6186.json?key=1d893519a4674aa4644704c3f8c86003249db597";
        HttpClient client = HttpClientBuilder.create().build();
        HttpPut put = new HttpPut(url);
        put.setHeader("Content-type","application/json;charset=utf-8");

        StringEntity params = new StringEntity(jo.toString(),"utf-8");
        put.setEntity(params);

        HttpResponse response = client.execute(put);
        System.out.println("Response Code:" + response.getStatusLine().getStatusCode());
        
    }

}
