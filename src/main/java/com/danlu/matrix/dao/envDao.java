package com.danlu.matrix.dao;

import java.util.List;
import java.util.Map;

import com.danlu.matrix.entity.db.envEntity;

public interface envDao {
    int deleteByPrimaryKey(Long envId);

    int insert(envEntity record);

    int insertSelective(envEntity record);

    envEntity selectByPrimaryKey(Long envId);

    int updateByPrimaryKeySelective(envEntity record);

    int updateByPrimaryKey(envEntity record);
    
    
}