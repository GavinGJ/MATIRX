package com.danlu.matrix.dao;

import com.danlu.matrix.entity.db.configHistoryEntity;

public interface configHistoryDao {
    int deleteByPrimaryKey(Long id);

    int insert(configHistoryEntity record);

    int insertSelective(configHistoryEntity record);

    configHistoryEntity selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(configHistoryEntity record);

    int updateByPrimaryKey(configHistoryEntity record);

}