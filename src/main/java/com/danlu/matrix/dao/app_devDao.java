package com.danlu.matrix.dao;

import com.danlu.matrix.entity.db.app_devEntity;

public interface app_devDao {
    int deleteByPrimaryKey(Integer id);

    int insert(app_devEntity record);

    int insertSelective(app_devEntity record);

    app_devEntity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(app_devEntity record);

    int updateByPrimaryKey(app_devEntity record);
}