package com.danlu.matrix.dao;

import java.util.List;
import java.util.Map;

import com.danlu.matrix.entity.db.appEntity;

public interface appDao {
    int deleteByPrimaryKey(Long appId);

    int insert(appEntity record);

    int insertSelective(appEntity record);

    appEntity selectByPrimaryKey(Long appId);

    int updateByPrimaryKeySelective(appEntity record);

    int updateByPrimaryKey(appEntity record);
    
    List<appEntity> getApp(Map<String, Object> map);
    
    int getAppCount(Map<String, Object> map);
}