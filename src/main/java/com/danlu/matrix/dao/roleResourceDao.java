package com.danlu.matrix.dao;

import com.danlu.matrix.entity.db.roleResourceEntity;

public interface roleResourceDao {
    int deleteByPrimaryKey(Integer roleResId);

    int insert(roleResourceEntity record);

    int insertSelective(roleResourceEntity record);

    roleResourceEntity selectByPrimaryKey(Integer roleResId);

    int updateByPrimaryKeySelective(roleResourceEntity record);

    int updateByPrimaryKey(roleResourceEntity record);
}