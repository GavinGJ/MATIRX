package com.danlu.matrix.dao;

import java.util.List;
import java.util.Map;

import com.danlu.matrix.entity.db.configEntity;

public interface configDao {
    int deleteByPrimaryKey(Long configId);

    int insert(configEntity record);

    int insertSelective(configEntity record);

    configEntity selectByPrimaryKey(Long configId);

    int updateByPrimaryKeySelective(configEntity record);

    int updateByPrimaryKeyWithBLOBs(configEntity record);

    int updateByPrimaryKey(configEntity record);

    int deleteStatus(Map<String, Object> map);
    
    List<configEntity> getApp(Map<String, Object> map);

    List<configEntity> getConfigLists(Map<String, Object> map);

    int insertLists(List<configEntity> configLists);

    List<String> getVersion(Map<String, Object> map);
    
    int getVersionCount(Map<String, Object> map);
    
    configEntity selectConfig(Map<String, Object> map);

    
}