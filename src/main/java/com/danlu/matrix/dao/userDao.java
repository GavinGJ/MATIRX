package com.danlu.matrix.dao;

import com.danlu.matrix.entity.db.userEntity;

public interface userDao {
    int deleteByPrimaryKey(Long userId);

    int insert(userEntity record);

    int insertSelective(userEntity record);

    userEntity selectByPrimaryKey(Long userId);

    int updateByPrimaryKeySelective(userEntity record);

    int updateByPrimaryKey(userEntity record);
}