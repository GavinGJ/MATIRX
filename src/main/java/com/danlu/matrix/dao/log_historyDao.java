package com.danlu.matrix.dao;

import java.util.Map;

import com.danlu.matrix.entity.db.log_historyEntity;

public interface log_historyDao {
    int deleteByPrimaryKey(Integer id);

    int insert(log_historyEntity record);

    int insertSelective(Map<String , Object> map);

    log_historyEntity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(log_historyEntity record);

    int updateByPrimaryKey(log_historyEntity record);
}