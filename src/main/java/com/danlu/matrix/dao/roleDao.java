package com.danlu.matrix.dao;

import com.danlu.matrix.entity.db.roleEntity;

public interface roleDao {
    int deleteByPrimaryKey(Integer roleId);

    int insert(roleEntity record);

    int insertSelective(roleEntity record);

    roleEntity selectByPrimaryKey(Integer roleId);

    int updateByPrimaryKeySelective(roleEntity record);

    int updateByPrimaryKey(roleEntity record);
}