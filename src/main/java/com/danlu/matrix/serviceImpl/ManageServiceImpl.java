package com.danlu.matrix.serviceImpl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.danlu.matrix.dao.appDao;
import com.danlu.matrix.dao.configDao;
import com.danlu.matrix.entity.db.appEntity;
import com.danlu.matrix.entity.db.configEntity;
import com.danlu.matrix.entity.request.manage.CopyPropertiesRequest;
import com.danlu.matrix.entity.request.manage.GetVersionRequest;
import com.danlu.matrix.entity.request.manage.ManageIndexRequest;
import com.danlu.matrix.entity.request.manage.MicroServicesDeleteRequest;
import com.danlu.matrix.entity.request.manage.VersionAddRequest;
import com.danlu.matrix.entity.request.manage.VersionDeleteRequest;
import com.danlu.matrix.entity.response.manage.CopyPropertiesResponse;
import com.danlu.matrix.entity.response.manage.GetVersionResponse;
import com.danlu.matrix.entity.response.manage.ManageIndexResponse;
import com.danlu.matrix.entity.response.manage.ManageIndexResponse.data;
import com.danlu.matrix.entity.response.manage.MicroServicesDeleteResponse;
import com.danlu.matrix.entity.response.manage.VersionAddResponse;
import com.danlu.matrix.entity.response.manage.VersionDeleteResponse;
import com.danlu.matrix.service.ManageService;

@Service("manageService")
public class ManageServiceImpl implements ManageService {

    @Resource
    private appDao    appDao;

    @Resource
    private configDao configDao;

    public int getApp(ManageIndexRequest manageIndexRequest, ManageIndexResponse manageIndexResponse) {
        Map<String, Object> map = new HashMap<String, Object>();
        List<data> dataLists = new ArrayList<data>();
        int startIndex = (Integer.parseInt(manageIndexRequest.getCurrentPage()) - 1)
                         * Integer.parseInt(manageIndexRequest.getPageCount());
        int endIndex = Integer.parseInt(manageIndexRequest.getPageCount());
        map.put("userId", manageIndexRequest.getUserId());
        map.put("envId", manageIndexRequest.getEnvId());
        map.put("startIndex", startIndex);
        map.put("endIndex", endIndex);

        List<appEntity> appList = appDao.getApp(map);
        int appCount = appDao.getAppCount(map);
        if (appList == null) {
            return 1;
        }
        for (int i = 0; i < appList.size(); i++) {
            data data = new data();
            data.setAppId(appList.get(i).getAppId().toString());
            data.setAppName(appList.get(i).getName().toString());
            dataLists.add(data);
        }
        manageIndexResponse.setTotalAppCount(String.valueOf(appCount));
        manageIndexResponse.setData(dataLists);
        manageIndexResponse.setCurrentPage(manageIndexRequest.getCurrentPage());
        return 0;
    }

    public int getVersion(GetVersionRequest getVersionRequest, GetVersionResponse getVersionResponse) {
        Map<String, Object> map = new HashMap<String, Object>();
        List<String> dataLists = new ArrayList<String>();

        int startIndex = (Integer.parseInt(getVersionRequest.getCurrentPage()) - 1)
                         * Integer.parseInt(getVersionRequest.getPageCount());
        int endIndex = Integer.parseInt(getVersionRequest.getPageCount());
        map.put("userId", getVersionRequest.getUserId());
        map.put("envId", getVersionRequest.getEnvId());
        map.put("appId", getVersionRequest.getAppId());
        map.put("startIndex", startIndex);
        map.put("endIndex", endIndex);

        List<String> versionList = configDao.getVersion(map);
        int versionCount = configDao.getVersionCount(map);
        if (versionList == null) {
            return 1;
        }

        dataLists.addAll(versionList);

        getVersionResponse.setTotalVersionCount(String.valueOf(versionCount));
        getVersionResponse.setVersion(dataLists);
        getVersionResponse.setCurrentPage(getVersionRequest.getCurrentPage());
        return 0;
    }

    public int MicroServicesDelete(MicroServicesDeleteRequest request,
                                   MicroServicesDeleteResponse response) {
        Map<String, Object> map = new HashMap<String, Object>();

        String envId = request.getEnvId();
        String appId = request.getAppId();

        map.put("appId", appId);
        map.put("envId", envId);
        // int result = appDao.deleteByPrimaryKey(appId);//删除微服务
        int deleteResult = configDao.deleteStatus(map);//更新数据表中的状态
        if (deleteResult == 0) {
            return 1;
        }
        return 0;
    }

    public int VersionAdd(VersionAddRequest request, VersionAddResponse response) {
        // TODO Auto-generated method stub
        return 0;
    }

    public int VersionDelete(VersionDeleteRequest request, VersionDeleteResponse response) {
        Map<String, Object> map = new HashMap<String, Object>();

        String envId = request.getEnvId();
        String appId = request.getAppId();
        String version = request.getVersion();

        map.put("appId", appId);
        map.put("envId", envId);
        map.put("version", version);
        int deleteResult = configDao.deleteStatus(map);//更新数据表中的记录为删除状态
        if (deleteResult == 0) {
            return 1;
        }
        return 0;
    }

    public int CopyProperties(CopyPropertiesRequest request, CopyPropertiesResponse response) {
        Map<String, Object> map = new HashMap<String, Object>();
        String time;
        Date date = new Date();
        time = (new SimpleDateFormat("yyyyMMddHHmmss")).format(date);

        map.put("versionSouece", request.getVersionNameCopySource());
        map.put("envIdSource", request.getEnvIdCopySource());
        map.put("appIdSource", request.getAppIdCopySource());

        String versionTarget = request.getVersionNameTarget();
        String envIdTarget = request.getEnvIdCopyTarget();
        String appIdTarget = request.getAppIdCopyTarget();

        configEntity configEntitySpurce = null;
        configEntity configEntityTarget = null;
        List<configEntity> configLists = new ArrayList<configEntity>();
        List<configEntity> getConfigLists = configDao.getConfigLists(map);

        for (int i = 0; i < getConfigLists.size(); i++) {
            configEntitySpurce = getConfigLists.get(i);
            configEntityTarget = new configEntity();
            configEntityTarget.setType(configEntitySpurce.getType());
            configEntityTarget.setStatus(configEntitySpurce.getStatus());
            configEntityTarget.setName(configEntitySpurce.getName());
            configEntityTarget.setValue(configEntitySpurce.getValue());
            configEntityTarget.setAppId(Long.parseLong(appIdTarget));
            configEntityTarget.setEnvId(Long.parseLong(envIdTarget));
            configEntityTarget.setVersion(versionTarget);
            configEntityTarget.setCreateTime(time);
            configEntityTarget.setUpdateTime(time);
            configLists.add(configEntityTarget);
        }

        int insert = configDao.insertLists(configLists);

        if (insert == 0) {
            return 1;
        }
        return 0;
    }

}
