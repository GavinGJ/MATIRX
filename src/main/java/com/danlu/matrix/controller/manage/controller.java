package com.danlu.matrix.controller.manage;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.danlu.dleye.servlet.DlServlet;

@RequestMapping("/testController")
public class controller {

    @RequestMapping(value="/test1",method=RequestMethod.GET)
    public void testController(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
       request.setAttribute("type", "SWITCH");
       request.setAttribute("action", "list");
        DlServlet dlServlet = new DlServlet();
        dlServlet.doGet(request, response);
        System.out.println(response);
    };
    

}
