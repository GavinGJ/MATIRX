package com.danlu.matrix.controller.manage;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.danlu.dleye.servlet.DlServlet;
import com.danlu.matrix.entity.request.manage.CopyPropertiesRequest;
import com.danlu.matrix.entity.request.manage.GetVersionRequest;
import com.danlu.matrix.entity.request.manage.ManageIndexRequest;
import com.danlu.matrix.entity.request.manage.MicroServicesDeleteRequest;
import com.danlu.matrix.entity.request.manage.VersionDeleteRequest;
import com.danlu.matrix.entity.response.manage.CopyPropertiesResponse;
import com.danlu.matrix.entity.response.manage.GetVersionResponse;
import com.danlu.matrix.entity.response.manage.ManageIndexResponse;
import com.danlu.matrix.entity.response.manage.MicroServicesDeleteResponse;
import com.danlu.matrix.entity.response.manage.VersionDeleteResponse;
import com.danlu.matrix.service.ManageService;
import com.danlu.matrix.util.constant.ServiceCode;

@Controller
@RequestMapping(value = "/manage")
public class ManageController {

   
    @Resource
    private ManageService manageService;

    @RequestMapping(value = "/test1", method = RequestMethod.GET)
    public void testController(HttpServletRequest request, HttpServletResponse response)
                                                                                        throws Exception {

        String result = "";
        BufferedReader in = null;
        try {
            String urlNameString = "http://123.57.228.235:8082/dleye-web/switch_list.html?ip=101.200.147.166&name=&port=9000";
            URL realUrl = new URL(urlNameString);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 建立实际的连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段
            for (String key : map.keySet()) {
                System.out.println(key + "--->" + map.get(key));
            }
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
            System.out.println(result);
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        request.setAttribute("type", "SWITCH");
        request.setAttribute("action", "list");

        DlServlet dlServlet = new DlServlet();
        dlServlet.init();
        dlServlet.doGet(request, response);
        System.out.println(response);
    };

    
    public void Test(){
        System.out.println("userId ");
    }
    /**
     * 进入管理页面.获得APP信息
     * @param request
     * @return
     */
    @RequestMapping(value = "/getApp", method = RequestMethod.POST)
    @ResponseBody
    public ManageIndexResponse getApp(@RequestBody ManageIndexRequest manageIndexRequest) {
        ManageIndexResponse manageIndexResponse = new ManageIndexResponse();
        int result = manageService.getApp(manageIndexRequest, manageIndexResponse);
        switch (result) {
            case 0:
                manageIndexResponse.setStatus(ServiceCode.CODE_0);
                break;
            case 1:
                manageIndexResponse.setStatus(ServiceCode.CODE_1);
                break;
        }
        return manageIndexResponse;
    }

    /**
     * 进入管理页面.获得版本信息
     * @param request
     * @return
     */
    @RequestMapping(value = "/getVersion", method = RequestMethod.POST)
    @ResponseBody
    public GetVersionResponse getVersion(@RequestBody GetVersionRequest getVersionRequest) {
        GetVersionResponse getVersionResponse = new GetVersionResponse();

        int result = manageService.getVersion(getVersionRequest, getVersionResponse);
        switch (result) {
            case 0:
                getVersionResponse.setStatus(ServiceCode.CODE_0);
                break;
            case 1:
                getVersionResponse.setStatus(ServiceCode.CODE_1);
                break;
        }
        return getVersionResponse;
    }

    /**
     * 删除微服务
     * @param request
     * @return
     */
    @RequestMapping(value = "/microServicesDelete", method = RequestMethod.POST)
    @ResponseBody
    public MicroServicesDeleteResponse microServicesDelete(@RequestBody MicroServicesDeleteRequest request) {
        MicroServicesDeleteResponse response = new MicroServicesDeleteResponse();

        int result = manageService.MicroServicesDelete(request, response);

        switch (result) {
            case 0:
                response.setStatus(ServiceCode.CODE_0);
                break;
            case 1:
                response.setStatus(ServiceCode.CODE_1);
                break;
        }
        return response;
    }

    /**
     * 版本号的增加
     * @param request
     * @return
     */
    /*
    @RequestMapping(value = "/versionAdd", method = RequestMethod.POST)
    @ResponseBody
    public ResultModel versionAdd(@RequestBody VersionAddRequest request) {
        ResultModel resultModel = new ResultModel();
        VersionAddResponse response = new VersionAddResponse();
    
        int result = manageService.VersionAdd(request, response);
    
        switch (result) {
            case 0:
                response.setSuccess(ServiceCode.CODE_0);
                break;
            case 1:
                response.setSuccess(ServiceCode.CODE_1);
                break;
        }
        resultModel.setModel(response);
        return resultModel;
    }*/

    /**
     * 版本号的删除
     * @param request
     * @return
     */
    @RequestMapping(value = "/versionDelete", method = RequestMethod.POST)
    @ResponseBody
    public VersionDeleteResponse versionDelete(@RequestBody VersionDeleteRequest request) {
        VersionDeleteResponse response = new VersionDeleteResponse();

        int result = manageService.VersionDelete(request, response);

        switch (result) {
            case 0:
                response.setStatus(ServiceCode.CODE_0);
                break;
            case 1:
                response.setStatus(ServiceCode.CODE_1);
                break;
        }
        return response;
    }

    /**
     * 复制文件
     * @return
     */
    @RequestMapping(value = "/copyProperties", method = RequestMethod.POST)
    @ResponseBody
    public CopyPropertiesResponse copyProperties(@RequestBody CopyPropertiesRequest request) {
        CopyPropertiesResponse response = new CopyPropertiesResponse();

        int result = manageService.CopyProperties(request, response);
        switch (result) {
            case 0:
                response.setStatus(ServiceCode.CODE_0);
                break;
            case 1:
                response.setStatus(ServiceCode.CODE_1);
                break;
        }
        return response;
    }

}
