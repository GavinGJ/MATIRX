package com.danlu.matrix.controller.manage;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.danlu.dleye.core.InfoManager;
import com.danlu.matrix.entity.response.manage.GetAllEyeResponse;

@Controller
@RequestMapping(value = "/Eye")
public class EyeController {

    private InfoManager infoManager;

    @RequestMapping(value = "/getAllEye", method = RequestMethod.GET)
    @ResponseBody
    public GetAllEyeResponse getAllEye() {
        GetAllEyeResponse getAllEyeResponse = new GetAllEyeResponse();

        getAllEyeResponse.setInfos(infoManager.getAllInfos());
        return getAllEyeResponse;
    }
    
    @RequestMapping(value = "/getBean" ,method = RequestMethod.POST )
    @ResponseBody
    public void get(){
        
    }

}
