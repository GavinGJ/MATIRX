package com.danlu.matrix.controller.manage;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.danlu.matrix.dao.appDao;
import com.danlu.matrix.dao.configDao;
import com.danlu.matrix.dao.envDao;
import com.danlu.matrix.dao.log_historyDao;
import com.danlu.matrix.dao.userDao;
import com.danlu.matrix.entity.db.appEntity;
import com.danlu.matrix.entity.db.configEntity;
import com.danlu.matrix.entity.db.envEntity;
import com.danlu.matrix.entity.db.userEntity;
import com.danlu.matrix.model.ParamModel;
import com.danlu.matrix.model.issue;
import com.danlu.matrix.util.constant.DateUtil;

@Component
@Aspect
public class TestAOP {
    @Resource
    private userDao   userDao;
    @Resource
    private appDao    appDao;
    @Resource
    private envDao    envDao;
    @Resource
    private configDao configDao;
    @Resource
    private log_historyDao logHistoryDao;

    @Pointcut("execution(* com.danlu.matrix.controller.manage.ManageController.*(..))")
    private void logMethod() {
    }

    @Around("logMethod()")
    public Object logAround(ProceedingJoinPoint point) {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> configMap = new HashMap<String, Object>();

        String desc = null;
        String methodName = null; //方法名
        Object[] params = null; //入參
        Object returnValue = null; //返回值
        try {
            Date date = new Date();
            String descTime = DateUtil.changeDateToString(date);
            methodName = "[" + point.getSignature().getDeclaringTypeName() + "."
                         + point.getSignature().getName() + "]";
            System.out.println("请求方法：" + methodName);
            params = point.getArgs();

            String json = JSONObject.toJSONString(params);
            String version = null;
            String versionCopySource = null;
            String versionCopyTarget = null;
            Long envId = 0l;
            Long userId = 0l;
            Long appId = 0l;
            Long appIdCopySource = 0l;
            Long appIdCopyTarget = 0l;
            Long envIdCopySource = 0l;
            Long envIdCopyTarget = 0l;

            List<ParamModel> jsons = JSON.parseArray(json, ParamModel.class);
            if (jsons.get(0).getUserId() != null) {
                userId = Long.parseLong(jsons.get(0).getUserId());
            }
            if (jsons.get(0).getAppId() != null) {
                appId = Long.parseLong(jsons.get(0).getAppId());
            }
            if (jsons.get(0).getEnvId() != null) {
                envId = Long.parseLong(jsons.get(0).getEnvId());
            }
            if (jsons.get(0).getVersion() != null) {
                version = jsons.get(0).getVersion();
            }
            if (jsons.get(0).getAppIdCopySource() != null) {
                appIdCopySource = Long.parseLong(jsons.get(0).getAppIdCopySource());
            }
            if (jsons.get(0).getAppIdCopyTarget() != null) {
                appIdCopyTarget = Long.parseLong(jsons.get(0).getAppIdCopyTarget());
            }
            if (jsons.get(0).getEnvIdCopySource() != null) {
                envIdCopySource = Long.parseLong(jsons.get(0).getEnvIdCopySource());
            }
            if (jsons.get(0).getEnvIdCopyTarget() != null) {
                envIdCopyTarget = Long.parseLong(jsons.get(0).getEnvIdCopyTarget());
            }
            if (jsons.get(0).getVersionNameCopySource() != null) {
                versionCopySource = jsons.get(0).getVersionNameCopySource();
            }
            if (jsons.get(0).getVersionNameTarget() != null) {
                versionCopyTarget = jsons.get(0).getVersionNameTarget();
            }

            map.put("userId", userId);
            map.put("appId", appId);
            map.put("envId", envId);
            map.put("version", version);
            userEntity user = userDao.selectByPrimaryKey(userId);
            appEntity app = appDao.selectByPrimaryKey(appId);
            envEntity env = envDao.selectByPrimaryKey(envId);
            appEntity appSourceEntity = appDao.selectByPrimaryKey(appIdCopySource);
            appEntity appTargetEntity = appDao.selectByPrimaryKey(appIdCopyTarget);
            envEntity envSourceEntity = envDao.selectByPrimaryKey(envIdCopySource);
            envEntity envTargetEntity = envDao.selectByPrimaryKey(envIdCopyTarget);
            //configEntity config = configDao.selectConfig(map);

            String userName = user.getName();
            if (methodName.contains("microServicesDelete")) {//删除微服务
                desc = descTime + "," + userName + "删除了" + env.getName() + "环境下的" + app.getName()
                       + "微服务";
                configMap.put("updateTime", descTime);
                configMap.put("updateBy", user.getUserId());
                configMap.put("envId", env.getEnvId());
                configMap.put("appId", app.getAppId());
                configMap.put("operation", "删除");
                configMap.put("desc", desc);
                
                
            } else if (methodName.contains("versionDelete")) {//删除版本
                desc = descTime + "," + userName + "删除了" + env.getName() + "环境下的" + app.getName()
                       + "微服务中的" + version + "版本";
                configMap.put("updateTime", descTime);
                configMap.put("updateBy", user.getUserId());
                configMap.put("envId", env.getEnvId());
                configMap.put("appId", app.getAppId());
                configMap.put("version", version);
                configMap.put("operation", "删除");
                configMap.put("desc", desc);
            } else if (methodName.contains("copyProperties")) {//复制文件
                desc = descTime + "," + userName + "从" + envSourceEntity.getName() + "环境下的"
                       + appSourceEntity.getName() + "微服务中版本号为：" + versionCopySource + "中的所有文件，复制到"
                       + envTargetEntity.getName() + "环境下的" + appTargetEntity.getName()
                       + "微服务中版本号为：" + versionCopyTarget + "中的所有文件";
                configMap.put("updateTime", descTime);
                configMap.put("updateBy", user.getUserId());
                configMap.put("envId", envSourceEntity.getEnvId());
                configMap.put("appId", appSourceEntity.getAppId());
                configMap.put("version", versionCopySource);
                configMap.put("operation", "复制");
                configMap.put("desc", desc);
            }
            logHistoryDao.insertSelective(configMap);
            send2Redmine(desc);
            returnValue = point.proceed(params);

        } catch (Throwable e) {
            System.out.println(methodName + "调用失败，抛出异常：" + e);
        }
        return returnValue;
    }

    public void send2Redmine(String desc) throws ClientProtocolException, IOException {
        JSONObject jo = new JSONObject();
        issue issue = new issue();
        issue.setNotes(desc);

        jo.put("issue", issue);

        String userKey = "key=1d893519a4674aa4644704c3f8c86003249db597";
        String url = "http://182.92.230.201:3000/issues/6186.json?" + userKey;
        HttpClient client = HttpClientBuilder.create().build();
        HttpPut put = new HttpPut(url);
        put.setHeader("Content-type", "application/json;charset=utf-8");

        StringEntity params = new StringEntity(jo.toString(), "utf-8");
        put.setEntity(params);

        HttpResponse response = client.execute(put);
        System.out.println("Response Code:" + response.getStatusLine().getStatusCode());

    }

    public void getMapByJson(String json) {
        Map mapTypes = JSON.parseObject(json);
        // 最外层解析  
        System.out.println("这个是用JSON类的parseObject来解析JSON字符串!!!");
        for (Object obj : mapTypes.keySet()) {
            System.out.println("key为：" + obj + "值为：" + mapTypes.get(obj));
        }
    }

}
