package com.danlu.matrix.TIJtest.TestChapter3.Test05;

public class Dog {


    public static void main(String[] args) {
       String args1 = "nihao";
       String args2 = "nihao";
       compareString(args1,args2);
    }

    public static void compareString(String args1, String args2) {
        System.out.println("args1==args2:" + args1 == args2);
        System.out.println("args1!=args2:" + args1 != args2);
        System.out.println("args1.equals(args2):" + args1.equals(args2));
    }

}
