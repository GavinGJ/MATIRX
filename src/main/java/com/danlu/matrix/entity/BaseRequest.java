package com.danlu.matrix.entity;

import java.io.Serializable;


public class BaseRequest implements Serializable {

    /** 默认当前页码 **/
    public static final int     PAGE_CURRENT                         = 1;
    /** 分页每页显示的记录数 **/
    public static final int     PAGE_SIZE                            = 100;
    
    private static final long serialVersionUID = 1820354282576704753L;

    /**
     * session user id
     */
    private String sessionId;

    /**
     * 会话验证标识
     */
    private String token;

    /**
     * 页码
     */
    public int page = this.PAGE_CURRENT;

    /**
     * 每页行数
     */
    public int rows = this.PAGE_SIZE;

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getPage() {
        if(this.page < 1) {
            return this.PAGE_CURRENT;
        } else {
            return this.page;
        }
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getRows() {
        if(this.rows < 1) {
            return this.PAGE_SIZE;
        } else {
            return this.rows;
        }
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

}
