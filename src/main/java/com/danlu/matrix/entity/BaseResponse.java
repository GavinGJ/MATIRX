package com.danlu.matrix.entity;

import java.io.Serializable;

public class BaseResponse implements Serializable {

    private static final long serialVersionUID = -8973044855637077568L;
    
    /**
     * 成功与否 0-成功 1-失败 其他-由具体接口定�?
     */
    private String success;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

}
