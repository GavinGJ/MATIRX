package com.danlu.matrix.entity.response.manage;

public class VersionDeleteResponse  {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
