package com.danlu.matrix.entity.response.manage;

public class CopyPropertiesResponse{
    
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
