package com.danlu.matrix.entity.response.manage;

import java.util.List;

import com.danlu.dleye.persist.base.Info;

public class GetAllEyeResponse {
    private List<Info> infos;

    public List<Info> getInfos() {
        return infos;
    }

    public void setInfos(List<Info> infos) {
        this.infos = infos;
    }

}
