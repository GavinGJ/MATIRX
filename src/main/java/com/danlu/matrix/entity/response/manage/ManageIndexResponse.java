package com.danlu.matrix.entity.response.manage;

import java.util.List;

public class ManageIndexResponse {

    private List<data> data;

    private String     status;

    private String     currentPage;

    private String     totalAppCount;

    public static class data {
        private String appId;

        private String appName;

        public String getAppId() {
            return appId;
        }

        public void setAppId(String appId) {
            this.appId = appId;
        }

        public String getAppName() {
            return appName;
        }

        public void setAppName(String appName) {
            this.appName = appName;
        }

    }

    public List<data> getData() {
        return data;
    }

    public void setData(List<data> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getTotalAppCount() {
        return totalAppCount;
    }

    public void setTotalAppCount(String totalAppCount) {
        this.totalAppCount = totalAppCount;
    }

}
