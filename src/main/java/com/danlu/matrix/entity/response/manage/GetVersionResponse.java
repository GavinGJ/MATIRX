package com.danlu.matrix.entity.response.manage;

import java.util.List;

public class GetVersionResponse {
    private List<String> version;

    private String       status;

    private String       currentPage;
    private String       totalVersionCount;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public List<String> getVersion() {
        return version;
    }

    public void setVersion(List<String> version) {
        this.version = version;
    }

    public String getTotalVersionCount() {
        return totalVersionCount;
    }

    public void setTotalVersionCount(String totalVersionCount) {
        this.totalVersionCount = totalVersionCount;
    }

}
