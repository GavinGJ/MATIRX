package com.danlu.matrix.entity.db;

public class roleResourceEntity {
    private Integer roleResId;

    private Integer roleId;

    private String  urlPattern;

    private String  urlDescription;

    private String  methodMask;

    private String  updateTime;

    public Integer getRoleResId() {
        return roleResId;
    }

    public void setRoleResId(Integer roleResId) {
        this.roleResId = roleResId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getUrlPattern() {
        return urlPattern;
    }

    public void setUrlPattern(String urlPattern) {
        this.urlPattern = urlPattern == null ? null : urlPattern.trim();
    }

    public String getUrlDescription() {
        return urlDescription;
    }

    public void setUrlDescription(String urlDescription) {
        this.urlDescription = urlDescription == null ? null : urlDescription.trim();
    }

    public String getMethodMask() {
        return methodMask;
    }

    public void setMethodMask(String methodMask) {
        this.methodMask = methodMask == null ? null : methodMask.trim();
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }
}