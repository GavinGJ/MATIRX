package com.danlu.matrix.entity.request.manage;

import com.danlu.matrix.entity.BaseRequest;

public class VersionDeleteRequest {

    private String version;

    private String userId;

    private String appId;

    private String envId;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getEnvId() {
        return envId;
    }

    public void setEnvId(String envId) {
        this.envId = envId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
