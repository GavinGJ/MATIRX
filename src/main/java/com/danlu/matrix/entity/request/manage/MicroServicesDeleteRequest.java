package com.danlu.matrix.entity.request.manage;

import com.danlu.matrix.entity.BaseRequest;

@SuppressWarnings("serial")
public class MicroServicesDeleteRequest extends BaseRequest {

    private String appId;
    
    private String userId;
    
    private String envId;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEnvId() {
        return envId;
    }

    public void setEnvId(String envId) {
        this.envId = envId;
    }
}
