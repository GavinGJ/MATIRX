package com.danlu.matrix.entity.request.manage;

import com.danlu.matrix.entity.BaseRequest;

@SuppressWarnings("serial")
public class ManageIndexRequest extends BaseRequest {

    private String envId;

    private String userId;

    private String currentPage;//当前页数

    private String pageCount;  //显示数据项总数

    public String getPageCount() {
        return pageCount;
    }

    public void setPageCount(String pageCount) {
        this.pageCount = pageCount;
    }

    public String getEnvId() {
        return envId;
    }

    public void setEnvId(String envId) {
        this.envId = envId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

}
