package com.danlu.matrix.util.constant;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    /**
     * 将 date 类型的时间转换为 String 类型
     * @param date
     * @return
     */
    public static String changeDateToString(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String dateString = df.format(date);
        return dateString;
    }
}
