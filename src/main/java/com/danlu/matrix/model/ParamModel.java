package com.danlu.matrix.model;

public class ParamModel {
    private String userId;
    private String appId;
    private String envId;
    private String version;
    private String versionNameCopySource;
    private String envIdCopySource;
    private String appIdCopySource;
    private String versionNameTarget;
    private String envIdCopyTarget;
    private String appIdCopyTarget;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getEnvId() {
        return envId;
    }

    public void setEnvId(String envId) {
        this.envId = envId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersionNameCopySource() {
        return versionNameCopySource;
    }

    public void setVersionNameCopySource(String versionNameCopySource) {
        this.versionNameCopySource = versionNameCopySource;
    }

    public String getEnvIdCopySource() {
        return envIdCopySource;
    }

    public void setEnvIdCopySource(String envIdCopySource) {
        this.envIdCopySource = envIdCopySource;
    }

    public String getAppIdCopySource() {
        return appIdCopySource;
    }

    public void setAppIdCopySource(String appIdCopySource) {
        this.appIdCopySource = appIdCopySource;
    }

    public String getVersionNameTarget() {
        return versionNameTarget;
    }

    public void setVersionNameTarget(String versionNameTarget) {
        this.versionNameTarget = versionNameTarget;
    }

    public String getEnvIdCopyTarget() {
        return envIdCopyTarget;
    }

    public void setEnvIdCopyTarget(String envIdCopyTarget) {
        this.envIdCopyTarget = envIdCopyTarget;
    }

    public String getAppIdCopyTarget() {
        return appIdCopyTarget;
    }

    public void setAppIdCopyTarget(String appIdCopyTarget) {
        this.appIdCopyTarget = appIdCopyTarget;
    }

}
