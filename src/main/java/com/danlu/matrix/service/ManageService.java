package com.danlu.matrix.service;

import java.util.List;

import com.danlu.matrix.entity.request.manage.CopyPropertiesRequest;
import com.danlu.matrix.entity.request.manage.GetVersionRequest;
import com.danlu.matrix.entity.request.manage.ManageIndexRequest;
import com.danlu.matrix.entity.request.manage.MicroServicesDeleteRequest;
import com.danlu.matrix.entity.request.manage.VersionAddRequest;
import com.danlu.matrix.entity.request.manage.VersionDeleteRequest;
import com.danlu.matrix.entity.response.manage.CopyPropertiesResponse;
import com.danlu.matrix.entity.response.manage.GetVersionResponse;
import com.danlu.matrix.entity.response.manage.ManageIndexResponse;
import com.danlu.matrix.entity.response.manage.MicroServicesDeleteResponse;
import com.danlu.matrix.entity.response.manage.VersionAddResponse;
import com.danlu.matrix.entity.response.manage.VersionDeleteResponse;

public interface ManageService {

    /**
     * ΢�����ɾ��
     * @param response 
     * @param request 
     * @return
     */
    public int MicroServicesDelete(MicroServicesDeleteRequest request,
                                   MicroServicesDeleteResponse response);

    /**
     * ΢����汾������
     * @param request
     * @param response
     * @return
     */
    public int VersionAdd(VersionAddRequest request, VersionAddResponse response);

    /**
     * ΢����汾��ɾ��
     * @param request
     * @param response
     * @return
     */
    public int VersionDelete(VersionDeleteRequest request, VersionDeleteResponse response);

    /**
     * 得到app微服务
     * @param manageIndexRequest
     * @param manageIndexResponse
     * @return
     */
    public int getApp(ManageIndexRequest manageIndexRequest,
                         ManageIndexResponse manageIndexResponse);


    
    /**
     * 得到某一个微服务下的所有版本
     * @param getVersionRequest
     * @param getVersionResponse
     * @return
     */
    public int getVersion(GetVersionRequest getVersionRequest,
                          GetVersionResponse getVersionResponse);
    
    /**
     * 复制文件
     * @param request
     * @param response
     * @return
     */
    public int CopyProperties(CopyPropertiesRequest request, CopyPropertiesResponse response);

}
